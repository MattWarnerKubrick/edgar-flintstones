# -*- coding: utf-8 -*-
"""
Created on Sun Aug  8 17:29:21 2021

@author: MatthewWarner
"""

import edgar_cleaner
import edgar_downloader
import edgar_sentiment_wordcount
import edgar_sentence_sentiment
import ref_data
import os
from datetime import date

def main():
    print('\n')
    # Obtain ticker and file base info from user
    ticker = str(input('Ticker: ') )
    print('\n\n')
    folder_base = str(input('Folder Base: '))    
    html_folder = folder_base + r'\html_folder'
    
    # Check if files already exist, create if not
    if not os.path.exists(html_folder):
        os.makedirs(html_folder)
    clean_text_folder = folder_base + r'\clean_text_folder'
    if not os.path.exists(clean_text_folder):
        os.makedirs(clean_text_folder)
    data_folder = folder_base + r'\data_folder'
    if not os.path.exists(data_folder):
        os.makedirs(data_folder)    
    print('\n\n') 
    
    # Download HTML files from SEC
    print('\nDownloading HTML Files...\n')
    edgar_downloader.download_files_10k(ticker, html_folder)
    print('\nDone.\n')
    
    # Clean HTMl files and save as txt
    print('\nCoverting HTML files to Text files and cleaning...\n')
    edgar_cleaner.write_clean_html_files(html_folder, clean_text_folder)
    print('\nDone.\n')
    
    # Obtain S&P100 data and check if ticker is present
    print('\nDownloading S&P 100 ticker information...\n')
    tickers_sp100 = ref_data.get_sp100()
    if ticker.upper() in tickers_sp100:
        print('\nDone. ',ticker.upper(), ' is in the S&P 100.\n')
    else:
        print('\nDone. ',ticker.upper(), ' is not in the S&P 100.\n')
    print('\nDownloading Yahoo Financial Data...\n')
    
    # use dates in file name convention to obtain appropriate amount of yahoo
    # Financial Data
    files = os.listdir(clean_text_folder)
    todays_date = date.today()
    first_year = todays_date.year
    for file in files:
        file_name_components = file.split('_')
        if file_name_components[0] == ticker:
            filing_year = int(file_name_components[2][:4])
            if filing_year < first_year:
                first_year = filing_year
    df = ref_data.get_yahoo_data(str(first_year)+'-01-01', str(todays_date), [ticker.upper()])
    df.to_csv(data_folder + '\\' + ticker + r'_yahoo_data.csv' , index=False)
    print('\nDone.\n')
    
    # Extract word and sentence level sentiment data
    print('\nExtracting Word-Level Document Sentiments...\n')
    edgar_sentiment_wordcount.write_document_sentiments(clean_text_folder, data_folder + '\\' + ticker + r'_word_sentiment.csv')
    print('\nDone.\n')
    print('\nExtracting Sentence-Level Document Sentiments...\n')
    edgar_sentence_sentiment.write_document_sentence_sentiments(clean_text_folder, data_folder + '\\' + ticker + r'_sentence_sentiment.csv')
    print('\nDone.\n')
    
    
if __name__ == '__main__':
    main()