# -*- coding: utf-8 -*-
"""
Created on Tue Aug 10 16:50:59 2021

@author: TomLewis
"""

import pandas as pd
from matplotlib import pyplot as plt
import ref_data

def graph_stock_prices(start_date, end_date, tickers):

    
    sp100= ref_data.get_sp100()
    
    df= ref_data.get_yahoo_data(start_date, end_date, tickers)
    
    fig, ax= plt.subplots(figsize=(12,6))
    
    for ticker in tickers:
        df_ticker = df[ df['Symbol'] == ticker]
        
        ax.plot(df_ticker['formatted_date'], df_ticker['price'], label= ticker)
        
    ax.set_title('Stock prices')
    ax.set_xlabel('Date')
    ax.set_ylabel('Adjusted closing price')
    
    ax.legend()
    
    plt.show()
        
        
def main():
    print("\n")
    start_date = input("Choose a start date (yyyy-mm-dd): ")
    print("\n")
    end_date = input("Choose an end date (yyyy-mm-dd): ")
    print("\n")
    ticker = ""
    tickers = []
    while ticker != "FINISH":
        ticker = input("Choose a company ticker to add or enter 'FINISH' to produce the graph" ": ")
        tickers.append(ticker.upper())
    tickers.pop(-1)
    graph_stock_prices(start_date, end_date, tickers)

if __name__ == "__main__":
	main()