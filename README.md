# EDGAR Project - Team Flintstones
## File Inventory:  
|File|Description|
|---|---|
|**daily_return_analysis.py**|Module of functions for producing analytical graphics of the produced data|
|**edgar_cleaner.py**|Module of functions for cleaning files and converting them to text files from HTML|
|**edgar_downloader.py**|Module of functions for downloading 10-K filings from the SEC database|
|**edgar_pipeline_DEMO.py**|Programme for showcasing all functions and abilities in this repository|
|**edgar_pipeline_single.py**|Programme for obtaining all data for a single company in the SEC database|
|**edgar_pipeline_SP100.py**|Programme for obtaining all data for all companies in the S&P100|
|**edgar_sentence_sentiment.py**|Module for collecting data on the sentiment of sentences in text documents|
|**edgar_sentiment_wordcount.py**|Module for collecting data on the sentiment of words in text documents|
|**graph_stock_data.py**|Module of function to produce graph of prices for a given stock over a given time|
|**ref_data.py**|Module for collecting financial reference data pertaining to the companies being analysed|
|**unittest1.py**|Module for performing unit tests on modules|
---
## Usage Instructions
### Data Collection and Pre-Processing
There are two ways in which a user can obtain data using these modules:
#### Individual Companies - edgar_pipeline_single.py
To obtain data for a single company, edgar_pipeline_single.py should be used. First, the user should open a command window and navigate to the folder where these modules are stored.

Next, they should type the following:
~~~
python edgar_pipeline_single.py
~~~
They will then prompted with the following:

![Image 1](README_Files/eps1.JPG)

Here, the user should input the stock market ticker for the company they wish to obtain data for and press ENTER. This will be followed by:

![Image 2](README_Files/eps2.JPG)

Here, the user should provide a path (e.g C:\Users\Me\Documents) to where they wish for the data to be stored. Three folders will be created at this location containing so be advised there should be sufficient space (~40Mb) available.

At this point, the programme will begin running. This may take several minutes so progress can be tracked via the progress bars in the command window.

![Image 3](README_Files/eps3.JPG)


#### All S&P 100 - edgar_pipeline_SP100.py
To obtain data for every company in the S&P 100, edgar_pipeline_SP100.py should be used. First, the user should open a command window and navigate to the folder where these modules are stored.

Next, they should type the following:
~~~
python edgar_pipeline_SP100.py
~~~
They will then prompted with the following:

![Image 4](README_Files/epsp1001.JPG)

Here, the user should provide a path (e.g C:\Users\Me\Documents) to where they wish for the data to be stored. Three folders will be created at this location containing so be advised there should be sufficient space (~5.3Gb) available. Unlike the single company version of the programme, a ticker is not required as these are sourced from an S&P100 database.

At this point, the programme will begin running. This may take several hour so progress can be tracked via the progress bars in the command window.

![Image 5](README_Files/epsp1002.JPG)

### Data Processing and Analysis

#### Producing Stock Price Graphs - graph_stock_data.py
To produce graphs of prices of specific stocks over time, the user should open a command window and navigate to the folder where these modules are stored.

Next, they should type the following:
~~~
python graph_stock_data.py
~~~

The user will then be prompted to supply two dates in the yyyy-mm-dd format for the start and end dates the users wants the graph to cover.

![Image 6](README_Files/gsd1.JPG)

After these have been inputted, next the user can provide the tickers of the stock they wish to see plotted. The user can provide multiple tickers one at a time and they are not case sensitive. Once the user has provided all the stocks they wish to see, they should type "FINISH" to begin the plotting stage.

![Image 7](README_Files/gsd2.JPG)

Soon after, a popup will open containing the requiested graph like that seen below.

![Image 8](README_Files/gsd3.jpeg)

#### Analysing Daily Return Data with Sentiment - daily_return_analysis.py
To produce analysis and graphs for the sentiment data against daily returns, the user should open a command window and navigate to the folder where these modules are stored.

Next, they should type the following:
~~~
python daily_return_analysis.py
~~~

The user will then be prompted to supply the file path to the folder where the anaylsis files (e.g word_sentiment.csv) are stored.

![Image 9](README_Files/dra1.JPG)

After this information is supplied, the programme will quickly return a set of graphs containing all the analysis data like those seen below:

![Image 10](README_Files/dra3.JPG)

Additionally, in the command window the programme will return a set of correlation values for the user to interpret.

![Image 11](README_Files/dra2.JPG)

### Showcasing

#### Demo Mode - edgar_pipeline_DEMO.py
There is a DEMO mode to showcase all the functions and abilities within this repository. The DEMO mode works by extrcating data for two companies and performing analysis on them. To use this, the user should open a command window and navigate to the folder where these modules are stored.

Next, they should type the following:
~~~
python edgar_pipeline_DEMO.py
~~~

At this point, the user will be prompted to supply a file base where all the files produced will be stored, as seen below:

![Image 12](README_Files/epd1.JPG)

This is the only input required. Once provided the programme will produce all the files, graphs and analysis given in the previous instructions.

### Validation

#### Unit Testing - unittest1.py
Unit testing can be performed to verify the functionality of some of the modules. To do this, the user should open a command window and navigate to the folder where these modules are stored.

Next, they should type the following:
~~~
python unittest1.py
~~~

No user inputs are required. After some time the user will be able to see the results of the tests in the command window.

