#ref_data module


from yahoofinancials import YahooFinancials
import pandas as pd
import datetime as dt

def get_yahoo_data(start_date, end_date, tickers):
    '''
    Function that collects historical financial data between given dates for provided tickers

    Parameters
    ----------
    start_date : string
        start of the period to be extracted. YYYY-MM-DD format
    end_date : string
        end of the period to be extracted. YYYY-MM-DD format
    tickers : iterable of strings
        the ticker symbols to be extracted

    Returns
    -------
    df : DataFrame
        the prices for the given dates and tickers
    '''
    
    df = pd.DataFrame([], columns = ['x'])  # Create a mostly empty DataFrame to concatenate data to
    
    date_time_end = dt.datetime.strptime(end_date, '%Y-%m-%d')  # Transform end date into datetime object
    date_time_end += dt.timedelta(days=15)                      # Add 15 days to the end date. This will be used to calculate returns later
    end_date = date_time_end.strftime('%Y-%m-%d')               # Change end date back into a string

    for ticker in tickers:
        
        
 
        data = YahooFinancials(ticker).get_historical_price_data(start_date, end_date, 'daily') 
        data_df = pd.DataFrame(data[ticker]['prices'])                         # Transform data into DataFrame
        data_df[['Symbol']] = ticker                                           # Add ticker to symbol column
        data_df.rename(columns = {'adjclose' : 'price'}, inplace = True)  # Rename adjusted close to price
        data_df = calc_returns(data_df, [1,2,3,5,10])                     # Returns must be calculated before concatenating data frames
        df = pd.concat([df, data_df] , axis = 0)
       
    df.drop(columns = ['date', 'open', 'close', 'x'], inplace = True)    # Drop unneeded and dummy columns
    df['formatted_date'] = pd.to_datetime(df['formatted_date']) 
    
    

    return(df)
        
        
        
def calc_returns(df, periods):
    '''
    Function to calculate returns if bought then sold after given number of days specified in periods

    Parameters
    ----------
    df : DataFrame
        DataFrame of prices for which daily_returns are to be calculated.
    periods : iterable of Ints
        The periods for which returns are to be calculated

    Returns
    -------
    df : DataFrame
        DataFrame with calculated returns
        
    Notes
    -------
    daily_returns are calculated for days of trading, not days overall. 
    '''
    
    for i in periods:
        
        df_shifted = df.shift(-i)                                      # Make a copy, shifted back i days
        df[[f'{i}daily_return']] = df_shifted['price'] - df['price']   # Effectively (price i days from now) - (price today)
        df2 = df.iloc[0:-15, :]                                        # Take last 15 rows off the table- only used for return calculations
        
    return df2
    

def get_sp100():
    '''
    Function to retrieve tickers symbols for the S&P100 companies
    
    Requirements
    ------------
    pandas

    Parameters
    ----------
    None

    Returns
    -------
    tickers : List
        Ticker symbols for S&P100 companies
    '''

    url = r"https://en.wikipedia.org/wiki/S%26P_100"
    table = pd.read_html(url)
    df = table[2]
    tickers = list(df["Symbol"])

    return(tickers)



def get_sentiment_word_dict():
    '''
    Function to get the Loughran-McDonald word sentiment dictionary
    
    Requirements
    ------------
    pandas

    Parameters
    ----------
    None

    Returns
    -------
    lm_sentiment_words : Dictionary
        Dictionary of word types and the words associated with them
    '''
    
    url = r"https://drive.google.com/uc?id=1moS1tkh_AJafpcIFpktaSvZwrzv5d4ix&export=download"
    df = pd.read_csv(url)
    
    word_types = ['Negative', 'Positive', 'Uncertainty', 'Litigious', 'Strong_Modal', 'Weak_Modal', 'Constraining']
    lm_sentiment_words = {}

    for i in word_types:
        mask = df[i] > 0
        lm_sentiment_words[i] = list(df["Word"][mask])
    
    for i in lm_sentiment_words:
        if '.' in i:            
            i= i.replace('.', '-') #get_sp100 tickers can have '.', but yahoofinancials uses '-'
        
    return(lm_sentiment_words)

