# -*- coding: utf-8 -*-
"""
Created on Sun Aug  8 12:50:54 2021

@author: XorsheedZanjani
"""
# Importing 
import os 
import ref_data
import pandas as pd 
from tqdm import tqdm


def write_document_sentiments(input_folder,output_file): 
    '''
    Function that takes all the clean text 10-k files in the input folder, counts the number of words in the 
    document belonging to a particular sentiment and outputs the results to the output file.

    Requirements
    --------------------
    os 
    edgar_downloader
    edgar_cleaner
    get_sentiment_word_dict
    pandas 
    
    Parameters
    --------------------
    input_folder (containing clean text files)
    output_file (csv file to store table)
    
    Returns
    --------------------
    csv file that contains a table of result, with a count of words belonging to a particular sentiment 
    
    
    '''
    
    
    
    column_names=['Symbol', 
                  'File Type',
                  'Filing Date', 
                  'Positive',
                   'Negative',
                   'Uncertainty',
                   'Litigious',
                   'Strong_Modal',
                   'Weak_Modal',
                   'Constraining',
                   'Word_Count']

    df_output = pd.DataFrame(columns=column_names) #initalize dataframe 

    
    
    # dictionary of word types
    dictionary = ref_data.get_sentiment_word_dict()
    
    #taking each text file from input folder 
    filenames = os.listdir(input_folder)

    #going through input files 
    for file in tqdm(filenames):
        if ".txt" in file:
            with open(input_folder + '\\'  + file, 'r') as f:
                text_file = f.read()
    
        
    
        #need to write counts to row for each text file 
            file_titles=file.split('_')
            
            df_output.loc[file]=[file_titles[0].upper(),file_titles[1],file_titles[2][0:10],0,0,0,0,0,0,0,0]

            for i in text_file.split(): 
                counted = False
                
                if i.upper() in dictionary['Positive']:
                    df_output.loc[file,'Positive']+=1
                    if not counted:
                        df_output.loc[file,'Word_Count']+=1
                        counted = True

                if i.upper() in dictionary['Negative']: 
                    df_output.loc[file,'Negative']+=1
                    if not counted:
                        df_output.loc[file,'Word_Count']+=1
                        counted = True

                if i.upper() in dictionary['Uncertainty']:
                    df_output.loc[file,'Uncertainty']+=1
                    if not counted:
                        df_output.loc[file,'Word_Count']+=1
                        counted = True

                if i.upper() in dictionary['Litigious']: 
                    df_output.loc[file,'Litigious']+=1
                    if not counted:
                        df_output.loc[file,'Word_Count']+=1
                        counted = True

                if i.upper() in dictionary['Strong_Modal']:
                    df_output.loc[file,'Strong_Modal']+=1 
                    if not counted:
                        df_output.loc[file,'Word_Count']+=1
                        counted = True

                if i.upper() in dictionary['Weak_Modal']: 
                    df_output.loc[file,'Weak_Modal']+=1
                    if not counted:
                        df_output.loc[file,'Word_Count']+=1
                        counted = True

                if i.upper() in dictionary['Constraining']: 
                    df_output.loc[file,'Constraining']+=1
                    if not counted:
                        df_output.loc[file,'Word_Count']+=1
                        counted = True



    #writing table into csv file 

    df_output.to_csv(output_file,index=False)

