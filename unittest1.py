# -*- coding: utf-8 -*-
"""
Created on Wed Aug  4 16:49:13 2021

@author: -
"""
import nltk
import unittest
import edgar_cleaner
import edgar_downloader
import ref_data
import os
import random
#%% These operations need to be performed in order before tests can be run

tickers_sp100= ref_data.get_sp100()
tickers_sp100= random.choices(tickers_sp100, k=10)

if not os.path.exists(r'./test_folder'):
    os.makedirs(r'./test_folder')

for i in tickers_sp100: 
    edgar_downloader.download_files_10k(i, r'./test_folder')
    

edgar_cleaner.write_clean_html_files(r'./test_folder', r'./test_folder')

df_returns= ref_data.get_yahoo_data('2017-01-01', '2021-01-01', tickers_sp100)
sentiment_dict= ref_data.get_sentiment_word_dict()

#%%
class TestEdgarDownloader(unittest.TestCase):
    

    
    def test_edgar_downloader(self):
        
        ticker= 'AAPl'
        file_path= r'./test_folder'
        edgar_downloader.download_files_10k(ticker, file_path)    
        print(os.listdir())
        file_list= [i for i in os.listdir() if '.html' in i]
        
        self.assertIsNotNone(file_list) #Test that a set of .html files have been downloaded
        
        
        for i in file_list:
            size= os.path.getsize(fr'./test_folder/{i}')/1024
            self.assertTrue(1000 < size < 30000) #test that the files are within an expected range of sizes
        
#%%



class TestEdgarcleaner(unittest.TestCase):
    
    def test_write_clean_html_files(self):
        
        file_list= [i for i in os.listdir() if '.txt' in i]
        
        self.assertIsNotNone(file_list) #Test that a set of .txt files have been created
        
        tags= ['<div>', '<font>', '<head>', '<html>', '<DOCUMENT>', '<body>', '<td>', '<tr>'] #common html tags
        
        for i in file_list:
            with open(fr'./test_folder/{i}', 'r', encoding="utf-8") as f:

                
                for tag in tags:
                    self.assertNotIn(tag, f) #test if any of the common html tags are in the file
        

#%%


class TestRefData(unittest.TestCase):
    
    def test_get_sp100(self):
        
        self.assertTrue(len(ref_data.get_sp100()) == 101)
        
        for i in tickers_sp100:
            self.assertTrue(len(i) <= 5)
            
    def test_get_yahoo_data(self):
        
        target_columns= ['high', 'low', 'volume', 'price', 'formatted_date', 'Symbol', '1daily_return',
                         '2daily_return', '3daily_return', '5daily_return', '10daily_return']
        
        self.assertTrue(list(df_returns.columns) == target_columns) #test if df_returns contains correct columns
        
        #self.assertTrue( len(df_returns) % 101 == 0) #test if the number of rows is a multiple of 101, i.e. an equal number of rows have been imported for each ticker
        #this test fails as the DOW ticker only has data starting in 2019
        #removing this from the test
        self.assertTrue( len(df_returns[df_returns['Symbol'] != 'DOW']) % 100 == 0)
        periods= [1, 2, 3, 5, 10]
        
        for i in periods:
            
            daily_return = df_returns['price'].iloc[i] - df_returns['price'].iloc[0]
            
            self.assertTrue(daily_return == df_returns[f'{i}daily_return'].iloc[0]) #check returns are being calculated correctly
            
            
    def test_get_sentiment_word_dict(self):
        
        target_keys= ['Negative', 'Positive', 'Uncertainty', 'Litigious', 'Strong_Modal', 'Weak_Modal', 'Constraining']
        
        self.assertTrue(target_keys == list(sentiment_dict.keys())) #check that the correct keys have been imported
        
        
        for i in sentiment_dict.values():
            self.assertIsNotNone(i) #check that each key has values
            
        

#%%
        
        

if __name__ == '__main__':
    unittest.main()
    
    
#%%

