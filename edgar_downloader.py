#Import modules/packages
from bs4 import BeautifulSoup
import requests
import datetime
import re

def write_page(url,file_path, is_pre2001=False):
    
    '''
    Author: Xorsheed Zanjani
    Date: July-August, 2021
    Function: write_page
    -----------------------------------------------------------------------
    Purpose: to scrape a webpage, and to write the contents into a chosen filepath 
    
    Requirements: 
    requests 
    re

    Inputs:
    - url: url of HTML script
    - file_path: specified file directory for HTML files to be downloaded into
    - is_pre2001: (True/False) specify "True" if the input 10-K is from year 2000 or before: triggers extra pre-processing stage.

    This function is used as part of a module called the 'edgar_downloader', to download raw 10-k filings from the SEC EDGAR website
    This function is to be used in conjunction with the download_files_10k(ticker,dest_folder) function

    '''    
    
    #Make request to url for 10-K html page
    user_agent = r'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36'
    r=requests.get(url,headers = {'User-Agent':user_agent})

    #Extra optional pre-processing stage to extract 10K document from combined pre-2000 files
    if is_pre2001:
        search_10K = re.search('<TEXT>(\n|.)*?</TEXT>', r.text)
        span_10K = search_10K.span()
        text_10K = r.text[span_10K[0]:span_10K[1]]
    
    #Else, simply convert to text if normal post-2000s 10-K file 
    else:
        text_10K = r.text

    #Write html to file
    with open(file_path, 'w', encoding="utf-8") as f:
        f.write(text_10K)


def download_files_10k(ticker,dest_folder):
    
    '''
    Author: Henry Roberts
    Date: July-August, 2021
    Function: download_files_10k
    -----------------------------------------------------------------------
    Purpose: to navigate the SEC EDGAR website and scrape html links for all 10-k files for the specified ticker, and input 
    into write_url function to download all 10-K files into destination folder.
    
    Requirements: 
    requests 
    datetime
    bs4 (BeautifulSoup)

    Inputs:
    - ticker: the stock ticker for the company of interest as a string e.g. "AAPL" for Apple
    - dest_folder: specified folder path (as string) for HTML files to be downloaded into 

    This function is used as part of a module called the 'edgar_downloader', to download raw 10-k filings from the SEC EDGAR website
    This function is to be used in conjunction with the write_page(url,file_path) function

    '''
    
    edgar_api_endpoint = r'https://www.sec.gov/cgi-bin/browse-edgar'
    today_date = datetime.datetime.now().strftime("%Y%m%d")

    #Set up parameters for EDGAR API query
    parameters = {'action':'getcompany',
                  'CIK': ticker,
                  'type':'10-k',
                  'dateb': today_date,
                  'owner':'exclude',
                  'start':'',
                  'output':'html',
                  'count':100}
    
    #Initial query to EDGAR file browser
    user_agent = r'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36'
    response = requests.get(edgar_api_endpoint,params = parameters,headers = {'User-Agent':user_agent})

    endpoint_soup = BeautifulSoup(response.content, 'html.parser')
    
    #Scrape list of links to page of filing details for each 10-K
    url_endings_list = []
    table_soup1 = endpoint_soup.find('table',{'class':'tableFile2'})
    for row in table_soup1.find_all('tr'):
        columns = row.find_all('td')
        if len(columns) !=0:
            if columns[0].text.strip() == '10-K':
                a = row.find('a',{'id':'documentsbutton'})
                url_endings_list.append(a['href'])

    archive_base_url = r'https://www.sec.gov'

    url_full_list = [archive_base_url + i for i in url_endings_list]

    #Scrape list of links to html pages of actual 10-K files
    final_link_endings  = []
    filing_date_list = []
    pre2001_mask = []
    
    for url in url_full_list:
        response = requests.get(url, headers = {'User-Agent':user_agent})
        full_soup = BeautifulSoup(response.content,'html.parser')
        table_soup3 = full_soup.find('table',{'class':'tableFile'})
        formgrouping_soup = full_soup.find('div',{'class':'formGrouping'})
        rows = formgrouping_soup.find_all('div')
        filing_date = rows[1].text.strip()
        
        for row in table_soup3.find_all('tr'):
            columns = row.find_all('td')
            
            if len(columns) !=0 and (columns[3].text.strip() in ['10-K','10-K405']) and columns[2].text.strip():
                a = row.find('a')
                
                #if filetype of link is "iXBRL", amends the url to return link HTML filetype instead
                if "iXBRL" in columns[2].text.strip():
                    link_ending = a['href'].replace('/ix?doc=','')
                    final_link_endings.append(link_ending)
                    pre2001_mask.append(False)
                    
                else:
                    final_link_endings.append(a['href'])
                    pre2001_mask.append(False) 
                filing_date_list.append(filing_date)
                break
            
            #For HTMLs that combine multiple files (pre-2001), note "True" in "pre2000_mask" for later pre-processing by write_url function
            elif len(columns) !=0 and columns[1].text.strip() == "Complete submission text file":
                a = row.find('a')
                final_link_endings.append(a['href'])
                pre2001_mask.append(True)
                filing_date_list.append(filing_date)               

    final_links = [archive_base_url + i for i in final_link_endings]

    #Scrape 10-K HTMLs and write as individual files to destination folder
    for i in range(len(final_links)):
        file_path = str(dest_folder) + f'/{ticker}_10-k_{filing_date_list[i]}.html'
        write_page(final_links[i],file_path, is_pre2001=pre2001_mask[i])



