"""
edgar_Cleaner.py

Created: Matt Warner 26/07/21


"""

import os
from bs4 import BeautifulSoup
from nltk.tokenize import sent_tokenize
import re
from tqdm import tqdm

def clean_html_text(html_text):
    '''
    Function to remove html tags and special characters from an html
    string.
    
    Requirements
    ------------
    re
    BeauitfulSoup
    nltk

    Parameters
    ----------
    html_text : String
        Raw, unencoded html text.

    Returns
    -------
    clean_text : String
        Procesed html text with special characters and tags removed.

    '''
    
    # Remove html formatting and extrcat pure-text
    soup = BeautifulSoup(html_text, 'html.parser')
    for script in soup(["script", "style"]):
        script.decompose()
    text = list(soup.stripped_strings)
    # Find Start of file after headers
    start = text.index('10-K')
    text=text[start:]
    clean_text = ''
    for token in text:
        # Clean text of special charcaters and excess whitespace.
        token = re.sub(r"[^a-zA-Z0-9\'\-\.]", ' ', token)
        token = re.sub(r"\s+", ' ', token)
        clean_text += token + ' '

    
    return clean_text


def write_clean_html_files(input_folder, dest_folder):
    '''
    Function to take all .html file in a directory and convert them to cleaned
    .txt files.
    
    Requirements
    ------------
    os

    Parameters
    ----------
    input_folder : string
        Path to directory containing .html files.
    dest_folder : string
        Path to directory where .txt files to be stored.

    Returns
    -------
    None.

    '''
    
    # List all files in directory
    filenames = os.listdir(input_folder)
    
    # Find all .html files and pass through clean_html_text() before writing to new file
    for file in tqdm(filenames):
        if ".html" in file:
            try:
                with open(dest_folder + '\\'  + file.replace('.html','.txt'), 'w', encoding='utf-8') as f:
                    f.write(clean_html_text(open(input_folder + '\\'  + file, 'r', encoding='utf-8').read()))
            except:
                pass
