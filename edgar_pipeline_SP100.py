# -*- coding: utf-8 -*-
"""
Created on Sun Aug  8 17:29:21 2021

@author: MatthewWarner
"""

import edgar_cleaner
import edgar_downloader
import edgar_sentiment_wordcount
import edgar_sentence_sentiment
import ref_data
import os
from datetime import date
from tqdm import tqdm

def main():
    # Allow User to define folder base
    folder_base = str(input('Folder Base: '))
    
    # Check if folders already exist, create them if not
    html_folder = folder_base + r'\html_folder'
    if not os.path.exists(html_folder):
        os.makedirs(html_folder)
    clean_text_folder = folder_base + r'\clean_text_folder'
    if not os.path.exists(clean_text_folder):
        os.makedirs(clean_text_folder)
    data_folder = folder_base + r'\data_folder'
    if not os.path.exists(data_folder):
        os.makedirs(data_folder)
    print('\n\n') 
    
    # Download the S&P100 tickers
    print('\nDownloading S&P 100 ticker information...\n')
    tickers_sp100 = ref_data.get_sp100()
    print('\nDone.\n')
    
    # Download HTML 10-ks for all companies, skipping any where downloading
    # fails
    print('\nDownloading HTML Files...\n')
    for ticker in tqdm(tickers_sp100):
        try:
            edgar_downloader.download_files_10k(ticker, html_folder)
        except:
            pass
    print('\nDone.\n')
    
    # Clean all HTML files and convert them to txt
    print('\nCoverting HTML files to Text files and cleaning...\n')
    edgar_cleaner.write_clean_html_files(html_folder, clean_text_folder)
    print('\nDone.\n')
    
    # Use dates in file naming convention to find relevent yahoo financial data
    print('\nDownloading Yahoo Financial Data...\n')
    files = os.listdir(clean_text_folder)
    for ticker in tqdm(tickers_sp100):
        todays_date = date.today()
        first_year = todays_date.year
        for file in files:
            file_name_components = file.split('_')
            if file_name_components[0] == ticker:
                filing_year = int(file_name_components[2][:4])
                if filing_year < first_year:
                    first_year = filing_year
        df = ref_data.get_yahoo_data(str(first_year)+'-01-01', str(todays_date), [ticker.upper()])
        df.to_csv(data_folder + '\\' + ticker + r'_yahoo_data.csv' , index=False)
    print('\nDone.\n')
    
    # Extract word and sentence sentiment frmo text files
    print('\nExtracting Word-Level Document Sentiments...\n')
    edgar_sentiment_wordcount.write_document_sentiments(clean_text_folder, data_folder + r'\word_sentiment.csv')
    print('\nDone.\n')
    print('\nExtracting Sentence-Level Document Sentiments...\n')
    edgar_sentence_sentiment.write_document_sentence_sentiments(clean_text_folder, data_folder + r'\sentence_sentiment.csv')
    print('\nDone.\n')
    
    
if __name__ == '__main__':
    main()