# -*- coding: utf-8 -*-
"""
Created on Tue Aug 10 11:15:11 2021

@author: OliviaDeSousa
"""
import os
import scipy.interpolate as si
import pandas as pd
import copy
import matplotlib.pyplot as plt

# Get list of file names
def get_file_names(file_path):
    file_names = []
    with os.scandir(file_path) as dirs:
        for entry in dirs:
            file_names.append(entry.name)
    return(file_names)
        


# Extract 10-k filing date
def dates_of_10k(word_count):
    tickers = list(set(word_count["Symbol"]))
    filing_dates = {}
    for i in tickers:
        mask = word_count["Symbol"] == i
        filing_dates[i] = list(word_count[mask]["Filing Date"])
    return(filing_dates)



# Create date ranges around 10-k filing dates
def filing_date_ranges(filing_dates, window_size):
    dif = pd.Timedelta(f"{window_size} days")
    date_ranges = {}
    
    for symbol in filing_dates:
        date_ranges[symbol] = []
        for date in filing_dates[symbol]:
            date = pd.to_datetime(date, dayfirst = True)
            date_ranges[symbol].append([str(date - dif)[:10], str(date)[:10], str(date + dif)[:10]])
    return(date_ranges)
    


# Create all our models
# Linear & cubic spline with knot at 10-k filing date
# coefficients are stored in [[before], [after]] format
# Where they are written the order of coefficients is as follows:
# a + bx    or    a + bx + cx^2 + dx^3 
def model_fitting(file_path, file_names, date_ranges):
    dailies = {"1daily_return":[], "2daily_return":[], 
               "3daily_return":[], "5daily_return":[], 
               "10daily_return":[]}
    all_the_data = {}

    # Iterate through each companies ticker symbol
    for symbol in date_ranges:
        all_the_data[symbol] = {"linear":copy.deepcopy(dailies), "cubic":copy.deepcopy(dailies)}
        df = pd.read_csv(f"{file_path}\\{symbol}_yahoo_data.csv")
        
        try:
            df.set_index("formatted_date", inplace = True)
        except:
            print("index already changed")
            
        # Iterate through all 10-k filing ranges for current ticker
        for i in date_ranges[symbol]:
            # Create date index
            all_daily_data = df.loc[i[0]:i[2]]
            dates = all_daily_data.index
            x = [pd.to_datetime(cur_day) - pd.to_datetime(i[1], dayfirst = True) for cur_day in dates]
            print(x)
            date_index = [cur_day._d for cur_day in x]
            
            # Iterate through each type of daily return
            for daily_type in dailies:
                daily = all_daily_data[daily_type]
    
                # Linear Spline data
                f = si.splrep(date_index, daily, k = 1, task = -1, t = [0])
                F = si.PPoly.from_spline(f)
                all_the_data[symbol]["linear"][daily_type].append([[F.c[1][2], F.c[0][0]], [F.c[1][2], F.c[0][2]]])
    
    
                # Cubic Spline data
                f = si.splrep(date_index, daily, k = 3, task = -1, t = [0])
                F = si.PPoly.from_spline(f)
                coef = F.c[:, 4]            
                all_the_data[symbol]["cubic"][daily_type].append([[coef[3], coef[2], coef[1], F.c[0][0]], [coef[3], coef[2], coef[1], F.c[0][5]]])
    return(all_the_data)


def analysis(file_path, window_size):
    file_names = get_file_names(file_path)
    word_count = pd.read_csv(f"{file_path}\\word_sentiment.csv")
    filing_dates = dates_of_10k(word_count)
    date_ranges = filing_date_ranges(filing_dates, window_size)
    all_the_data = model_fitting(file_path, file_names, date_ranges)
    return(all_the_data)
    


def get_changes(model_dict, word_count):
    tickers = list(model_dict.keys())
    changes_linear= pd.DataFrame(columns= ['daily_return', 'positive_density', 'negative_density','uncertainty_density', 'litigious_density',  'change'])
    changes_cubic= pd.DataFrame(columns= ['daily_return', 'positive_density', 'negative_density',
                                          'change_beta1', 'change_beta2', 'change_beta3'])
    
    
    for ticker in tickers:
        
        df= word_count[word_count['Symbol'] == ticker]
        

                
        for daily in [1, 2, 3, 5, 10]:
            
            for filing in range(len(df)):
                
                positive_density= df['Positive'].iloc[filing] / df['Word_Count'].iloc[filing]
                negative_density= df['Negative'].iloc[filing] / df['Word_Count'].iloc[filing]
                uncertainty_density= df['Uncertainty'].iloc[filing] / df['Word_Count'].iloc[filing]
                litigious_density= df['Litigious'].iloc[filing] / df['Word_Count'].iloc[filing]
                
                # For linear models
                model= model_dict[ticker]['linear'][f'{daily}daily_return']
                
                change= (model[filing][1][1] - model[filing][0][1])
                
                         
                df_row_linear= (pd.DataFrame([[daily, positive_density, 
                                        negative_density, uncertainty_density, litigious_density, change]], 
                                        columns= ['daily_return', 'positive_density', 
                                                 'negative_density', 'uncertainty_density', 
                                                 'litigious_density', 'change']))
                
                changes_linear= pd.concat([changes_linear, df_row_linear])
                
                #For cubic models
                model= model_dict[ticker]['cubic'][f'{daily}daily_return']
                
                cubic_changes=[]
                

                change_beta1= (model[filing][1][1] - model[filing][0][1])
                change_beta2= (model[filing][1][2] - model[filing][0][2])
                change_beta3= (model[filing][1][3] - model[filing][0][3])
                         
                df_row_cubic= (pd.DataFrame([[daily, positive_density, 
                                        negative_density, change_beta1, change_beta2, change_beta3]], 
                                        columns= ['daily_return', 'positive_density', 
                                                 'negative_density', 'change_beta1',
                                                 'change_beta2', 'change_beta3']))
                 
                changes_cubic= pd.concat([changes_cubic, df_row_cubic])
                
                
                
    changes_linear.reset_index(inplace= True, drop= True)
    changes_cubic.reset_index(inplace= True, drop= True)
    
    ninety_fifth_quantile= changes_linear['change'].quantile(q=0.95)
    fifth_quantile= changes_linear['change'].quantile(q=0.05)
    
    changes_linear =changes_linear.apply(lambda row: row if (row['change'] > fifth_quantile and row['change'] < ninety_fifth_quantile) else None, axis = 1)
                    
    return changes_linear, changes_cubic
    

def graph_changes_linear(changes_linear):
   
    fig, ax = plt.subplots(5, 4, figsize= (30, 30))
    
    dailies = {1:1, 2:2, 3:3, 4:5, 5:10}
    
    correlations_negative={'1daily_correlation' : '', '2daily_correlation' : '', '3daily_correlation' : '', 
                  '5daily_correlation' : '', '10daily_correlation' : '' }
    correlations_positive={'1daily_correlation' : '', '2daily_correlation' : '', '3daily_correlation' : '', 
              '5daily_correlation' : '', '10daily_correlation' : '' }

    for i in range(1, 6):
        
        df = changes_linear[changes_linear['daily_return'] == dailies[i]]
        ax[i-1][0].scatter(df['positive_density'], df['change'])
        
        ax[i-1][0].set_title(f'{dailies[i]}daily_return')
        ax[i-1][0].set_xlabel('positive_density')
        ax[i-1][0].set_ylabel('change')
        
        ax[i-1][1].scatter(df['negative_density'], df['change'])
        
        ax[i-1][1].set_title(f'{dailies[i]}daily_return')
        ax[i-1][1].set_xlabel('negative_density')
        ax[i-1][1].set_ylabel('change')
        
        ax[i-1][2].scatter(df['uncertainty_density'], df['change'])
        
        ax[i-1][2].set_title(f'{dailies[i]}daily_return')
        ax[i-1][2].set_xlabel('uncertainty_density')
        ax[i-1][2].set_ylabel('change')

        ax[i-1][3].scatter(df['litigious_density'], df['change'])
        
        ax[i-1][3].set_title(f'{dailies[i]}daily_return')
        ax[i-1][3].set_xlabel('litigious_density')
        ax[i-1][3].set_ylabel('change')
        
        change= df['change']
        negative_density= df['negative_density']
        positive_density= df['positive_density']
        
        correlation_negative= change.corr(negative_density)
        correlations_negative[f'{dailies[i]}daily_correlation'] = correlation_negative
        
        correlation_positive= change.corr(positive_density)
        correlations_positive[f'{dailies[i]}daily_correlation'] = correlation_positive
     
    plt.show()
    return correlations_positive, correlations_negative

def graph_changes_cubic(changes_cubic):  
    dailies = {1:1, 2:2, 3:3, 4:5, 5:10}
    
    for i in range(1, 4):
        change_target = f'change_beta{i}'
        
        fig, ax = plt.subplots(5, 2, figsize= (30, 30))
    
        for i in range(1, 6):
            
            df = changes_cubic[changes_cubic['daily_return'] == dailies[i]]
            
            ax[i-1][0].scatter(df['positive_density'], df[change_target])
            
            ax[i-1][0].set_title(f'{dailies[i]}daily_return')
            ax[i-1][0].set_xlabel('positive_density')
            ax[i-1][0].set_ylabel(change_target)
            
            ax[i-1][1].scatter(df['negative_density'], df[change_target])
            
            ax[i-1][1].set_title(f'{dailies[i]}daily_return')
            ax[i-1][1].set_xlabel('negative_density')
            ax[i-1][1].set_ylabel(change_target)
            
    plt.show()
           


def graph_correlations(correlations_positive, correlations_negative):
    
    days= [1, 2, 3, 5, 10]
    
    fig, ax= plt.subplots(figsize=(12,6))
    
    ax.plot(days, list(correlations_positive.values()), label = 'positive word density correlation', color = "red")
    ax.plot(days, list(correlations_negative.values()), label = 'negative word density correlations', color = "blue")
    
    ax.scatter(days, list(correlations_positive.values()), color = "red")
    ax.scatter(days, list(correlations_negative.values()), color = "blue")
    
    ax.set_title('Correlations between positive/negative word count and daily return')
    ax.set_xlabel(' (x) daily return')
    ax.set_ylabel('correlation value')
    ax.legend()
    ax.set_ylim([-1, 1])
    plt.show()
            
        
def main():
    print("\n")
    file_path = input('File path to sentiment data folder: ')
    print("\n")

    model_dict = analysis(file_path, 10)
    word_count = pd.read_csv(f"{file_path}\\word_sentiment.csv")

    changes_linear, changes_cubic = get_changes(model_dict, word_count)

    correlations_positive, correlations_negative= graph_changes_linear(changes_linear)
    graph_changes_cubic(changes_cubic)

    print('Correlations_positive:')
    for i in correlations_positive:
        print(i, ':' ,correlations_positive[i])


    print('Correlations_negative:')
    for i in correlations_negative:
        print(i, ':' ,correlations_negative[i])
    
    graph_correlations(correlations_positive, correlations_negative)
    
if __name__ == '__main__':
    main()
