#from textblob import TextBlob
import os
import nltk
from nltk.tokenize import sent_tokenize
import pandas as pd
from nltk.sentiment.vader import SentimentIntensityAnalyzer
from tqdm import tqdm


def write_document_sentence_sentiments(input_folder, output_file):
    
    '''
    
    Function to analyse a folder of .txt files and create a .csv file analysing
    each files sentiment on a sentence level.
    
    Requirements
    ------------
    textblob
    os
    pandas
    nltk

    Parameters
    ----------
    input_folder : String
        Address of folder where .txt files to be analysised are.
        
    output_file : String
        Address and file name of the csv where the analysis info will be stored.

    Returns
    -------
    None
    
    '''
    
    # Check NLTK libraries present
    try:
        nltk.data.find('tokenizers/punkt')
    except LookupError:
        nltk.download('punkt')
    try:
        nltk.data.find('sentiment/vader_lexicon.zip')
    except LookupError:
        nltk.download('vader_lexicon')
    
    # List all files in directory
    filenames = os.listdir(input_folder)
    
    df = pd.DataFrame(columns=['Symbol','ReportType','FilingDate','Positive Sentences', 'Neutral Sentences', 'Negative Sentences', 'Total Sentences'])#,
                               #'Total Positivity', 'Total Negitivity'])
    
    for file in tqdm(filenames):
        if ".txt" in file:
            with open(input_folder + '\\'  + file, 'r') as f:
                text = f.read()
            
            filename_info = file.split('_')
            df.loc[file] = [filename_info[0].upper(), filename_info[1], filename_info[2][:10],0,0,0,0]#,0,0]
            sents = sent_tokenize(text)
            
            for sent in sents:
                score = SentimentIntensityAnalyzer().polarity_scores(sent)
                neg = score['neg']
                pos = score['pos']
                
                if pos > neg:
                    df.loc[file, 'Positive Sentences'] += 1
                elif pos < neg:
                    df.loc[file, 'Negative Sentences'] += 1
                else:
                    df.loc[file, 'Neutral Sentences'] += 1
                    
            df.loc[file, 'Total Sentences'] = df.loc[file, 'Positive Sentences'] + df.loc[file, 'Negative Sentences'] + df.loc[file, 'Neutral Sentences']
                
    df.to_csv(output_file, index=False)
            
    pass

def test_code():
    input_folder = r'C:\Users\MatthewWarner\Documents\Python Assessments\edgar-flintstones\test_folder'
    output_file = r'C:\Users\MatthewWarner\Documents\Python Assessments\edgar-flintstones\test_folder\results.csv'
    
    write_document_sentence_sentiments(input_folder, output_file)
    
if __name__ == '__main__':
    test_code()